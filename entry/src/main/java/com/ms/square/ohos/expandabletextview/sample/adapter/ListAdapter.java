/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ms.square.ohos.expandabletextview.sample.adapter;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.util.List;

/**
 * Version 1.0
 * ModifiedBy
 * date 2021-03-15 10:13
 * description 列表数据适配器
 */
public abstract class ListAdapter<T> extends BaseItemProvider {
    private List<T> data;
    private Context ct;
    private int itemId;

    public ListAdapter(Context ct, int itemId, List<T> data) {
        this.data = data;
        this.ct = ct;
        this.itemId = itemId;
    }

    public abstract void convert(ViewHolder viewHolder, T item, int position);

    @Override
    public int getCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public T getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ViewHolder viewHolder;
        Component itemView = LayoutScatter.getInstance(ct).parse(itemId, componentContainer, false);
        viewHolder = new ViewHolder(itemView);
        convert(viewHolder, getItem(i), i);
        return itemView;
    }
}
