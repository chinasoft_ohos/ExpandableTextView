/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ms.square.ohos.expandabletextview.sample.slice;

import com.ms.square.ohos.expandabletextview.sample.ResourceTable;
import com.ms.square.ohos.expandabletextview.sample.adapter.MainPagerAdapter;
import com.ms.square.ohos.expandabletextview.sample.utils.ViewCreateHelper;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.PageSlider;
import ohos.agp.components.TabList;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.service.WindowManager;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Version 1.0
 * ModifiedBy
 * date 2021-03-15 10:13
 * description 主界面
 */
public class MainAbilitySlice extends AbilitySlice implements TabList.TabSelectedListener,
        PageSlider.PageChangedListener {
    private static final HiLogLabel HI_LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0xFFFFF, "MainAbilitySlice");
    private static final int GRAY_COLOR = 211;
    private static final int WHITE_COLOR = 0;
    private TabList tabList;
    private PageSlider pageSlider;
    private DirectionalLayout rootLayout;

    @Override
    public void onStart(Intent intent) {
        // 设置状态栏颜色
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(getColor(ResourceTable.Color_purple_700));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
        initData();
    }

    private void initView() {
        tabList = (TabList) findComponentById(ResourceTable.Id_tab_list);
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pager);
        tabList.addTabSelectedListener(this);
        pageSlider.addPageChangedListener(this);
        tabList.setFixedMode(true);
        rootLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_root);
    }

    /**
     * 设置tabList
     */
    private void initData() {
        try {
            String[] tabsTitle = getResourceManager().getElement(ResourceTable.Strarray_tab_title).getStringArray();
            for (String title : tabsTitle) {
                TabList.Tab tab = tabList.new Tab(getContext());
                tab.setText(title);
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(new RgbColor(0, 0, 0, 0));
                tab.setBackground(shapeElement);
                tabList.addTab(tab);
            }
            tabList.selectTabAt(0);
            pageSlider.setProvider(new MainPagerAdapter(initPageSliderViewData()));
            pageSlider.setCurrentPage(0);
            pageSlider.setReboundEffect(true);
            pageSlider.setCentralScrollMode(true);
        } catch (IOException | NotExistException | WrongTypeException e) {
            HiLog.debug(HI_LOG_LABEL, e.getMessage());
        }
    }

    private ArrayList<Component> initPageSliderViewData() {
        if (tabList == null) {
            HiLog.debug(HI_LOG_LABEL, "initPageSliderViewData tabList is null");
            return null;
        }
        ArrayList<Component> pages = new ArrayList<>();
        int pageSize = tabList.getTabCount();
        if (pageSize < 1) {
            return pages;
        }
        ViewCreateHelper viewCreateHelper = new ViewCreateHelper(getContext());
        for (int tabIndex = 0; tabIndex < pageSize; tabIndex++) {
            pages.add(viewCreateHelper.createView(tabList.getTabAt(tabIndex).getText(), tabIndex));
        }
        return pages;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onPageSliding(int i, float v, int i1) {
    }

    @Override
    public void onPageSlideStateChanged(int i) {
    }

    @Override
    public void onPageChosen(int i) {
        if (tabList.getSelectedTab().getPosition() != i) {
            tabList.selectTabAt(i);
        }
        if (i == 0) {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(new RgbColor(GRAY_COLOR, GRAY_COLOR, GRAY_COLOR, GRAY_COLOR));
            rootLayout.setBackground(shapeElement);
        } else {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(new RgbColor(WHITE_COLOR, WHITE_COLOR, WHITE_COLOR, WHITE_COLOR));
            rootLayout.setBackground(shapeElement);
        }
    }

    @Override
    public void onSelected(TabList.Tab tab) {
        if (pageSlider.getCurrentPage() != tab.getPosition()) {
            pageSlider.setCurrentPage(tab.getPosition());
        }
    }

    @Override
    public void onUnselected(TabList.Tab tab) {
    }

    @Override
    public void onReselected(TabList.Tab tab) {
    }
}
