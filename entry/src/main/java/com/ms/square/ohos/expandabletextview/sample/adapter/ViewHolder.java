/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ms.square.ohos.expandabletextview.sample.adapter;

import ohos.agp.components.Component;

import java.util.HashMap;

/**
 * Version 1.0
 * ModifiedBy
 * date 2021-03-15 10:13
 * description ViewHolder
 */
public class ViewHolder {
    private Component component;
    private HashMap<Integer, Component> views;

    ViewHolder(Component itemView) {
        this.component = itemView;
        views = new HashMap<>(0);
        component.setTag(this);
    }

    public <T extends Component> T getView(int viewId) {
        Component view = views.get(viewId);
        if (view == null) {
            view = component.findComponentById(viewId);
            views.put(viewId, view);
        }
        return (T) view;
    }
}
