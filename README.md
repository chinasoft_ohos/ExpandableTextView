# ExpandableTextView

#### 项目介绍
- 项目名称：可以展开/折叠的Text
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现可以展开/折叠的Text控件
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release v0.1.3

#### 效果演示
<img src="https://gitee.com/chinasoft_ohos/ExpandableTextView/raw/master/img/demo.gif"></img>

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:ExpandableTextView:1.0.2')
    ......  
 }
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

使用该库非常简单，只需查看提供的示例的源代码。（查看ViewCreateHelper.java中在ScrollView和ListContainer中的使用）
```示例XML
<com.ms.square.ohos.expandabletextview.ExpandableTextView

        ohos:id="$+id:expandable"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:orientation="vertical"
        app:animAlphaStart="0.7"
        app:animDuration="300"
        app:collapseIndicator="$media:ic_expand_less_black_12dp"
        app:expandIndicator="$media:ic_expand_more_black_12dp"
        app:expandToggleOnTextClick="true"
        app:expandToggleType="$integer:ImageButton"
        app:mExpandCollapseToggleId="$string:ExpandCollapseToggleId"
        app:mExpandableTextId="$string:ExpandableTextId"
        app:maxCollapsedLines="4">

        <Text
            ohos:id="$+id:expandable_text"
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:component_description="$string:ExpandableTextId"
            ohos:layout_alignment="horizontal_center"
            ohos:left_margin="10vp"
            ohos:multiple_lines="true"
            ohos:right_margin="10vp"
            ohos:text="$string:dummy_text2"
            ohos:text_alignment="left"
            ohos:text_color="#666666"
            ohos:text_font="sans-serif-light"
            ohos:text_size="16vp"
            ohos:top_margin="8vp"/>

        <Image
            ohos:id="$+id:expand_collapse"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:background_element="$ohos:graphic:button_bg_transparent"
            ohos:component_description="$string:ExpandCollapseToggleId"
            ohos:image_src="$media:ic_expand_more_black_12dp"
            ohos:layout_alignment="right"
            ohos:right_margin="20vp"
            ohos:top_margin="20vp"/>
    </com.ms.square.ohos.expandabletextview.ExpandableTextView>
```

```java
ExpandableTextView expandableTextView = (ExpandableTextView)
                            component.findComponentById(ResourceTable.Id_expandable);
expandableTextView.setText(slice.getString(ResourceTable.String_dummy_text2));
```
另外，您可以选择在布局xml文件中设置以下属性，以自定义ExpandableTextView的行为。
1. maxCollapsedLines （默认为8）当TextView折叠时允许显示的最大文本行数

2. animDuration （默认为300毫秒）扩展/折叠动画的持续时间

3. animAlphaStart （默认值为0.7f）动画开始时TextView的Alpha值（注意）如果要禁用Alpha动画，请将此值设置为1。

4. expandDrawable 自定义一个可绘制的设置为ImageButton以展开TextView

5. collapseDrawable 自定义一个可绘制的设置为ImageButton以折叠TextView

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.2

#### 版权和许可信息

    Copyright 2014 Manabu Shimobe
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
